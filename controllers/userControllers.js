// business logic
// including model methods

// User model
const { model } = require('mongoose')
const User = require('./../models/User')
const auth = require('../auth')
const bcrypt = require('bcrypt')
const { JsonWebTokenError } = require('jsonwebtoken')

module.exports.getAllUsers = () => {
  // difference between findOne() & ficnd()
  // findOne() returns one document
  // find(query, fieldprojection) return an array of documents []
  return User.find().then((result) => {
    return result ? result : error
  })
}

module.exports.getUserProfile = (reqBody) => {
  const { email, password } = reqBody

  return User.findOne({ email: email }).then((result, error) => {
    // console.log(result)

    if (result == null) {
      console.log('email null')
      return false
    } else {
      let isPwCorrect = bcrypt.compareSync(password, result.password) //boolean bec it compares two arguments

      if (isPwCorrect == true) {
        //return json web token
        //invoke the function which creates the token upon logging in
        // requirements in creating a token:
        // if password matches from existing pw from db
        return { access: auth.createAccessToken(result) }
        // return auth.createAccessToken(result)
      } else {
        return false
      }
    }
  })
  // mini activity
  // using findById(), look for the matching docunent and
  // return the matching document to the client
}

module.exports.checkEmail = async (email) => {
  // find the matching document in the databse using email
  // by using Model() method.
  // send back the response to the client

  // const result = await User.findOne({ email: email })
  // if (result != null) email.send(false)
  // return result == null ? email.send(true) : res.send(error)

  console.log(result) //null

  if (result !== null) {
    return false
  } else {
    if (result === null) {
      //send back the response to the client
      return true
    } else {
      return error
    }
  }
}

module.exports.registerUser = (reqBody) => {
  // save/create a new user document
  // using .save() method to save document to the database
  // console.log(reqBody)

  // how to use object destructuring
  // why? to make distinct variables for each property w/o using dot notation
  // const {properties} = < object reference >
  const { firstname, lastname, email, password, mobileNo, age } = reqBody
  // console.log(firstName)

  const newUser = new User({
    firstname: firstname,
    lastname: lastname,
    email: email,
    password: password,
    mobileNo: mobileNo,
    age: age
  })

  // save the newUser object to the database
  return newUser.save().then((result, error) => {
    console.log(result)
    // return error ? error : result
    return result ? true : error
  })
}

module.exports.login = (reqBody) => {
  const { email, password } = reqBody

  return User.findOne({ email: email }).then((result, error) => {
    console.log(result)

    if (result == null) {
      console.log(`email null`)
      return false
    } else {
      let isPasswordCorrect = bcrypt.compareSync(password, result.password) // rerturns boolean

      if (isPasswordCorrect === true) {
        // return json web token
        // invoke the function in which creates the token upon logging in
        // requirements in creating a token:
        // if password matches from existing password to database
        return { access: auth.createAccessToken(result) }
      } else {
        return false
      }
    }
  })
}

// module.exports.getUserProfileById = (id) => {
// 	return User.findById({_id: }).then((result, error) => {

// 	});
// };

module.exports.verify = (req, res, next) => {
  let token = req.headers.authorization
  console.log(token)
  let slicedToken = token.slice(7, token.length)
  if (typeof slicedToken !== 'undefined') {
    return jwt.verify()
  } else {
    res.send(false)
  }
  return typeof slicedToken !== 'undefined'
    ? jwt.verifty(slicedToken, secret, (error, data) => {
        // if (err) {
        // 	res.send({ auth: failed });
        // } else {
        // 	next();
        // }
        return error ? res.send({ auth: failed }) : next()
      })
    : res.send(false)

  // jwt.verify(token, secret, function)
}
