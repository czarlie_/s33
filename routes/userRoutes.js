// create routes
const express = require('express')

// import to use .Router() method
// because .Router() is an express method
// Router() handles the requests
const router = express.Router()

// User Controller to use invoke()
const userControllers = require('../controllers/userControllers')
const auth = require('../auth')
const { verify } = require('jsonwebtoken')
const User = require('../models/User')

router.get('/', (req, res) => {
  userControllers.getAllUsers().then((result) => res.send(result))
})

router.get('/details', auth.verify, (req, res) => {
  // console.log(req.headers.authorization);
  let userData = auth.decode(req.headers.authorization)
  userControllers.getUserProfile(userData).then((result) => res.send(result))
})

//register route
router.post('/register', (req, res) => {
  //expecting data/object coming from request body
  userControllers.registerUser(req.body).then((result) => res.send(result))
})

router.post('/login', (req, res) => {
  userControllers.login(req.body).then((result) => res.send(result))
})

router.post('/email-exists', (req, res) => {
  //invoke the function here
  userControllers.checkEmail(req.body.email).then((result) => res.send(result))
})

/*mini activity*/
//create a route to get all users
// endpoint "/"
//method get
//model method find()
//output should be array of users
// router.get("/:id", (req, res) => {
// 	userControllers.getUserProfileById({}).then((result) => res.send(result));
// });

module.exports = router
