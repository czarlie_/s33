// json we token
// sign - function that creates a token
// verify - function that checks if there's presence of token
// decode - this function that decodes the token

const jwt = require('jsonwebtoken')
const secret = 'MagandaAko'

module.exports.createAccessToken = (user) => {
  const data = {
    id: user._id,
    email: user.email,
    isAdmin: user.isAdmin
  }
  return jwt.sign(data, secret, {})
}

module.exports.decode = (token) => {
  // jwt.decode(token, {});
  let slicedToken = token.slice(7, token.length)

  console.log(jwt.decode(slicedToken, { complete: true }).payload)
}
